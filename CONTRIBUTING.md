## Contributing

This project uses the [forking workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow). 
Please fork this project and submit pull requests to the [primary repository](https://git.doit.wisc.edu/adi-ia/rpc-netid-php).
It is expected that unit tests are created for new and updated functionality. Pull requests should be descriptive.