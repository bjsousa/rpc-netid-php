# Release Process #

1. User has cloned the [primary repository](https://git.doit.wisc.edu/adi-ia/rpc-netid-php/) and has write access to it.
2. User runs the following command: `php phing.phar release -Dversion=1.2.3 [-Dbranch=some-alternate-branch]` The branch argument is optional and defaults to `master`.
3. The designated branch is checked out.
4. A tag is created with the name equal to the version number with no extra characters
5. The repository is pushed to origin with all tags. 

The end result is a new tag in the remote repository equal to the version number. Composer clients will now pick up on the new version number.
