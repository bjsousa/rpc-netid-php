<?php

use edu\wisc\doit\CertificateService;

require_once 'vendor/Autoload.php';

/**
 * Unit tests for the edu\wisc\doit\CertificateService class.
 * 
 * These tests rely on self-signed certificates in the /src/test/resources folder.
 */

class CertificateServiceTest extends PHPUnit\Framework\TestCase {
	
	private $cert;
	private $key;
	private $pem;
	private $badCert;
	private $encryptedKey;
	
	function setUp() {
		parent::setUp();
		$this->cert = realpath( __DIR__ . '/resources/testClientCert.crt' );
		$this->key = realpath( __DIR__ . '/resources/testClientCert.key' );
		$this->pem = realpath( __DIR__ . '/resources/testClientCert.pem' );
		$this->badCert = realpath( __DIR__ . '/resources/notacert' );
		$this->encryptedKey = realpath( __DIR__ . '/resources/encryptedKey.pem' );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 100
	 */
	function validateCertificate_FileDoesNotExist() {
		CertificateService::validateCertificate( "asdf.crt" );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 101
	 */
	function validateCertificate_FileIsNotX509Format() {
		CertificateService::validateCertificate( $this->badCert );
	}
	
	/** @test */
	function validateCertificate_CertificateIsValid() {
		$this->assertTrue( CertificateService::validateCertificate( $this->cert ) );
	}
	
	/** @test */
	function validateCertificate_CertifiateIsValidInPEMFormatWithKey() {
		$this->assertTrue( CertificateService::validateCertificate( $this->pem ) );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 100
	 */
	function validatePrivateKey_FileDoesNotExist() {
		CertificateService::validatePrivateKey( 'asdf.pem' );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 101
	 */
	function validatePrivateKey_FileIsNotX509() {
		CertificateService::validatePrivateKey( $this->badCert );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 101
	 */
	function validatePrivateKey_PemIsMissingKey() {
		CertificateService::validatePrivateKey( $this->cert );
	}
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 101
	 */
	function validatePrivateKey_KeyIsEncrypted() {
		CertificateService::validatePrivateKey( $this->encryptedKey );
	}
	
	/** @test */
	function validatePrivateKey_ValidKeyInPemWithCertificate() {
		$this->assertTrue( CertificateService::validatePrivateKey( $this->pem ) );
	}
	
	/** @test */
	function validatePrivateKey_ValidKey() {
		$this->assertTrue( CertificateService::validatePrivateKey( $this->key ) );
	}
	
	
	/**
	 * @test
	 * @expectedException edu\wisc\doit\CertificateServiceException
	 * @expectedExceptionCode 100
	 */
	function createPem_PathIsUnwritable() {
		@CertificateService::createPem( $this->cert, $this->key, "./asdf/dne" );
	}
	
	/** @test valid certificate, key, and writable path */
	function createPem_ValidArguments() {
		$temp = realpath( __DIR__ . '/resources' ) . '/pemtest.pem';
		$this->assertTrue( CertificateService::createPem( $this->cert, $this->key, $temp ) );
		$this->assertFileExists( $temp );
		$this->assertTrue( CertificateService::validateCertificate( $temp ) );
		$this->assertTrue( CertificateService::validatePrivateKey( $temp ) );
		unlink( $temp );
	}
	
}
